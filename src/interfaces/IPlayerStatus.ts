export default interface IPlayerStatus {
  nameBase64: string,
  connectedTime: Date,
  frags: number,
  colors: number
}