export default interface IServerDef {
  id: string,
  name: string,
  game: string,
  gameType: string,
  connecthostport: string,
  location: string,
  description: string,
  failedQuery: number
}