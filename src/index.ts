import * as router from 'aws-lambda-router'
import * as Joi from 'joi'
import * as db from './database'
import * as state from './state'
import {query} from './query'
import axios from 'axios'
import IServerDef from './interfaces/IServerDef'
import IServerStatus from './interfaces/IServerStatus';

const addServerSchema = Joi.object({
  serverId: Joi.string().required(),
  name: Joi.string().required(),
  game: Joi.string().required(),
  gameType: Joi.string(),
  connecthostport: Joi.string().required(),
  location: Joi.string().required(),
  description: Joi.string()
})


export const handler = router.handler({
  proxyIntegration: {
    debug: process.env.ROUTER_DEBUG === 'true',
    routes: [ {
      path: '/api/server/ping',
      method: 'GET',
      action: (request, context) => {
        return JSON.stringify({ message: 'pong' })
      }
    },{
      // request-path-pattern with a path variable:
      path: '/api/server/query',
      method: 'POST',
      // we can use the path param 'id' in the action call:
      action: (request, context) => {
        return query()
          .then(() => {
            console.log('Query complete.')
            return {message: 'query complete'} as any
          })
          .catch(err => {
            throw {reason: 'ServerError', message: JSON.stringify({message: 'Failed to add server: ' + err.message})} 
          })
      }
    }, {
      // request-path-pattern with a path variable:
      path: '/api/server',
      method: 'GET',
      // we can use the path param 'id' in the action call:
      action: (request, context) => {
        return db.getAll()
          .then(servers => servers.map((
            {definition, status}) => ({
              connecthostport: definition.connecthostport,
              name: definition.name,
              location: definition.location,
              game: definition.game,
              lastQuery: status.lastQuery,
              players: status.players,
              map: status.map,
              maxPlayers: status.maxPlayers
            })))
          .then(results => {
            return {
              statusCode: 200,
              headers: {
                'Content-Type': 'application/json',
                'Cache-Control': `max-age=10`,
              },
              body: JSON.stringify(results)
            }
          })
          .catch(err => {
            throw {reason: 'ServerError', message: 'Failed query servers: ' + err.message} 
          })
      }
    }, {
      path: '/api/server',
      method: 'POST',
      action: (request, context) => {
        const body = request.body as any
        const validate = addServerSchema.validate(body)
        if (validate.error) {
          throw {reason: 'BadRequest', message: JSON.stringify(validate.error)}
        }
        
        const serverDef:IServerDef = {
          id: body.serverId,
          name: body.name,
          game: body.game,
          gameType: body.gameType,
          connecthostport: body.connecthostport,
          location: body.location,
          description: body.description,
          failedQuery: 0
        }

        // validate connunication and connecthostport is the caller
        return axios.post(`https://${serverDef.connecthostport}/mastervalidate`, {id: serverDef.id})
          .then(validateResponse => {
            if (validateResponse.data.message === 'OK') {
              return db.addServerDef(serverDef)
                .then(() => query())
                .then(() => {
                  console.log("Server update from " + body.name)
                  return {message: "server added"}
                })
                .catch((err) => {
                  
                  throw {reason: 'ServerError', message: 'Failed to add server: ' + err.message} 
                })
            } else {
              throw {reason: 'ServerError', message: 'Invalid message returned'}
            }
          })
          .catch(er => {
            console.log("Server failed validateion on  " + serverDef.connecthostport + ': ' + er.message)
            
            throw {reason: 'ServerError', message: 'Failed update: ' + er.message}
          })
      }
    }],
    errorMapping: {
      'BadRequest': 400,
      'NotFound': 404,
      'MyCustomError': 429,
      'ServerError': 500
    }
  }
})