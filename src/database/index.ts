import IServerDef from '../interfaces/IServerDef'
import IServerStatus from '../interfaces/IServerStatus'
import * as AWS from 'aws-sdk'
import {promisify} from 'util'
import { v4 as uuidv4 } from 'uuid';
// var AWS = require("aws-sdk");

const tableName = 'ServerDef'
const docClient = new AWS.DynamoDB.DocumentClient();


export const init = (options: any): Promise<void> => {
  return Promise.resolve()
}


export const remove = (serverDef: IServerDef) => {
  const address = serverDef.connecthostport
  var params = {
    TableName: tableName,
    Key: { address }
  }
  return promisify(docClient.delete.bind(docClient))(params)
}

export const updateServerStatus = (address: string, status: IServerStatus) => {
  var params = {
    TableName: tableName,
    Key: {
      "address": address
    },
    UpdateExpression: "set serverStatus = :s",
    ExpressionAttributeValues:{
      ":s": status
    },
    ReturnValues:"UPDATED_NEW"
  }

  return promisify(docClient.update.bind(docClient))(params)
}



export const addServerDef = (definition: IServerDef) : Promise<void> =>  {
  const address = definition.connecthostport

  return promisify(docClient.get.bind(docClient))({TableName: tableName, Key: { address }})
    .then(serverDef => {
      var params = {
        TableName: tableName,
        Key: {
          "address": address
        },
        UpdateExpression: "set serverStatus = :s, definition = :d",
        ExpressionAttributeValues:{
          ":s": "",
          ":d": definition
        },
        ReturnValues:"UPDATED_NEW"
      }
      
      return promisify(docClient.update.bind(docClient))(params)
    })
    .catch(err => {
      var params = { TableName: tableName, Item: { address, definition }}
      return promisify(docClient.put.bind(docClient))(params)
    })
}

export const getAll = () : Promise<Array<{definition:IServerDef, status: IServerStatus}>> => {
  var params = {
    TableName: tableName
  }
  return promisify(docClient.scan.bind(docClient))(params)
    .then(data => data.Items
      .filter(item => !!item.definition)
      .map(item => {
        return {
          definition: item.definition,
          status: item.serverStatus
        }
      }))
}