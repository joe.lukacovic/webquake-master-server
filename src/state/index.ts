import IServerStatus from '../interfaces/IServerStatus'

let serverStatus: IServerStatus[] = []

export const getServerStatus = () :IServerStatus[] => serverStatus
export const setServerStatus = (newServerStatus: IServerStatus[]) => serverStatus = newServerStatus