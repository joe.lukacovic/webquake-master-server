import * as db from '../database'
import * as webQuake from './webquake'

export const query = async () => {
  const servers = await db.getAll()

  const promises = servers
    .filter(s => !!s.definition)
    .map(server => webQuake.query(server)
      .then(status => db.updateServerStatus(server.definition.connecthostport, status)))
      
  return Promise.all(promises)
}