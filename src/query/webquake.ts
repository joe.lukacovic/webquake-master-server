import axios from 'axios'
import * as db from '../database'
import IServerStatus from '../interfaces/IServerStatus'
import IServerDef from '../interfaces/IServerDef'
import * as state from '../state'

const waitTimes = [
  0,
  5 * 1000,
  10 * 1000,
  30 * 1000,
  60 * 1000,
  60 * 1000,
  60 * 1000,
  5 * 60000, // 5 minutes
  15 * 60000, // 15 minutes
]

const emptyServerStatus = (serverDef: IServerDef): IServerStatus => ({
  connecthostport: serverDef.connecthostport,
  lastQuery: 0,
  failedQueries: 0,
  players: [],
  map: '',
  serverSettings: '',
  maxPlayers: 0,
  version: 0
})

const updateServerStatus = (oldSs: IServerStatus, newSs: IServerStatus): IServerStatus => {
  return {
    ...oldSs,
    ...newSs,
    lastQuery: 0,
    failedQueries: 0
  }
}

export const query = ({definition, status}): Promise<IServerStatus> => {
 
  const url = `https://${definition.connecthostport}/status`
  const serverStatus = status || emptyServerStatus(definition)
  const now = new Date().getTime()
  
  if (serverStatus.failedQueries > 0) {
    const waitTime = waitTimes[serverStatus.failedQueries]
    if (!waitTime) {
      db.remove(definition)
      return Promise.resolve(null)
    }
    if (serverStatus.lastQuery + waitTime > now) {
      return Promise.resolve(serverStatus)
    }
  }

  return axios.get(url, {timeout: 1000})
    .then(response => {
      return updateServerStatus(serverStatus, response.data)
    })
    .catch(e => {
      serverStatus.lastQuery = now,
      serverStatus.failedQueries++;
      return serverStatus
    })
}